package com.innopolis.attestation;

import java.io.*;
import java.util.Scanner;
import java.util.function.Consumer;

public class UserRepositoryFileImpl implements UserRepositoryFile {

    private final File dataFile;
    private final File idFile;
    private final File tempFile;


    public UserRepositoryFileImpl(File dataFile, File idFile, File tempFile) {
        this.dataFile = dataFile;
        this.idFile = idFile;
        this.tempFile = tempFile;
    }

    @Override
    public User findById(int id) throws FileNotFoundException {
        try (Scanner scanner = new Scanner(dataFile)){
            while (scanner.hasNext()){
                String line = scanner.nextLine();
                String[] blocks = line.split("\\|");
                int idCandidate = Integer.parseInt(blocks[0]);
                if (idCandidate == id) {
                    User user = new User();
                    user.setId(Integer.parseInt(blocks[0]));
                    user.setName(blocks[1]);
                    user.setSurname(blocks[2]);
                    user.setAge(Integer.parseInt(blocks[3]));
                    user.setHasWork(Boolean.parseBoolean(blocks[4]));
                    return user;
                }
            }
        }
        return null;
    }

    @Override
    public void create(User user) throws IOException {
        int currentId = checkLastId();
        currentId++;
        writeLastId(currentId);
        user.setId(currentId);
        writeToFile(dataFile, user + "\n", true);
    }

    @Override
    public void update(User user) throws IOException {
        useTempFile(user.getId(), user + "\n");
    }

    @Override
    public void delete(int id) throws IOException {
        useTempFile(id, null);
    }

    private int checkLastId() throws FileNotFoundException{
        try (Scanner scanner = new Scanner(idFile)) {
            String textNumber = scanner.nextLine();
            return Integer.parseInt(textNumber);

        }
    }

    private void writeLastId(int id) throws IOException {
        writeToFile(idFile, String.valueOf(id), false);
    }
    private void writeToFile(File file,String string, boolean appened) throws IOException{
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, appened));
        bufferedWriter.write(string);
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    private void useTempFile(int userId, String writeElse) throws IOException {
        tempFile.createNewFile();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(tempFile, false));
        try (Scanner scanner = new Scanner(dataFile)){
            while (scanner.hasNext()){
                String line = scanner.nextLine();
                String[] blocks = line.split("\\|");
                int idCandidate = Integer.parseInt(blocks[0]);
                if (idCandidate != userId) {
                    bufferedWriter.write(line + "\n");
                } else if (writeElse != null) {
                    bufferedWriter.write(writeElse);


                }
            }
        }
        bufferedWriter.flush();
        bufferedWriter.close();
        dataFile.delete();
        tempFile.renameTo(dataFile);
        tempFile.delete();
    }
}
